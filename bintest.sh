#!/usr/bin/env sh

# Author: Ofer Caspi
# Date: 2019-12-21

if [ $# -eq 0 ] || [ $1 = "-h" ] || [ $1 == "--help" ]
then
  echo "Usage: bintest.sh patch-file base-ref target-ref test-command\n\nProgram to find the first commit on which the test fails\n\n\
EXAMPLE:\nGiven a patch file testfile.patch that contains a python file named test.py with a test called my_test:\n\tbintest.sh testfile.patch master HEAD pytest test.py::my_test"
  exit 0
fi

orig_branch=$(git rev-parse --abbrev-ref HEAD)

commits=($(git log $2..$3 --reverse --pretty=format:"%h"))
commits=("${commits[@]%%:*}")

min=0
max=${#commits[@]}

while [ $min -lt $max ]
do
  index=`expr '(' "$min" + "$max" + 1 ')' / 2`
  commit_hash=${commits[$index]}
  
  git checkout -q $commit_hash
  git apply --whitespace nowarn $1

  $(${@:4} > /dev/null)
  test_result=$?

  git apply -R $1
  
  if [ $test_result -eq 0 ]
  then
    min=$index
  else
    max=`expr $index - 1`
  fi
done


if [ $test_result -eq 0 ]
then
  echo "bintest: Guilty commit not found..."
else
  echo "$(git show -s --pretty=short)"  
fi

git checkout -q "$orig_branch"
exit `expr 1 - $test_result`
