# Bintest

Ever wondered on which commit an error was introduced? Your wait is over!

Bintest will find a commit that introduced a bug by running any test on commits between two branches, with a binary search strategy.

For example, say we have a Python project with some code in `main.py`, and we're currently on a branch called `my_branch`.
Let's add a test in a new file called `test_main.py`:

```python
from main import func

def my_test():
    assert func(1) == 1
```

Let's create a patch containing our test called `example.patch`:

```sh
$ git add test_main.py
$ git diff > example.patch
```

That's it! Let's use Bintest with our patch, and tell it how to test our code:

```sh
$ ./bintest.sh example.patch master HEAD pytest test_main.py::my_test
commit e36d2c86f315d389c92d8937b2324e777b75559e
Author: Guilty Person <person@jail.com>

    I like introducing bugs and confessing on commit messages
```

### Notes

* The patch can contain anything that will not cause a merge conflict when applied to the commits we're going to scan. For that reason it's best to just create a new file with our test.
